
export const configServer = {
    host : process.env.host,
    port : process.env.port,
    routes : {cors : process.env.cors === 'true'? true : false }
}
export const databaseConfig = {
    name : process.env.connection_name,
    type : process.env.database_type,
    host : process.env.database_host,
    port : Number(process.env.database_port),
    schema : process.env.database_schema,
    connectionTimeout : Number(process.env.database_timeout),
    username : process.env.database_username,
    password : process.env.database_password,
    database : process.env.database,
    synchronize : process.env.synchronize === 'true' ? true : false,
}

export const secretkey = `${process.env.SECRET_ACCESS_TOKEN}`
