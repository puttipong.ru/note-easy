
import { Entity, Column, PrimaryColumn } from "typeorm"

@Entity({ name: 'note_category'})
export default class note_category {
    
    @PrimaryColumn({
        length:16,
        nullable:false
    })
    uuid:string;

    @PrimaryColumn({
        length:16,
        nullable:false
    })
    categoryid: string;

    @Column({
        length: 32,
        nullable: false
    })
    categoryname: string;

};
