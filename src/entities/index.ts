import note from './note'
import customer from './customer'
import note_history from './note_history'
import note_category from './note_category'

export default [ note,note_history,customer,note_category ]