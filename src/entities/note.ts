
import { Entity, Column, PrimaryColumn } from "typeorm"

@Entity({ name: 'note'})
export default class note {

    @PrimaryColumn({
        length: 16,
        nullable: false
    })
    noteid: string;
    
    @Column({
        length:16,
        nullable:false
    })
    uuid: string;
    
    @Column({
        length: 16,
        nullable: false
    })
    categoryid: string;

    @Column({
        length:32,
        nullable:false
    })
    note_name : string;

    @Column({ nullable: true})
    note_content : string;
    
    @Column({ 
        type:'boolean',
        nullable: true
    })
    isDeleted : boolean;

};
