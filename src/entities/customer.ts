
import { Entity, Column, PrimaryColumn } from "typeorm"

@Entity({ name: 'customer'})
export default class customer {

    @PrimaryColumn({
        length:16,
        nullable:false
    })
    uuid: string;

    @Column({
        length: 32,
        nullable: false
    })
    username: string;

    @Column({ 
        nullable: false
    })
    hash : string;

    @Column({
        nullable: false
    })
    salt:string
    
};
