
import { Entity, Column, PrimaryColumn } from "typeorm"

@Entity({ name: 'note_history'})
export default class note_history {

    @PrimaryColumn({
        length: 16,
        nullable: false
    })
    noteid: string;

    @Column({ nullable: true})
    note_content : string;

    @PrimaryColumn({ type: "timestamp" })
    timeStamp: Date;

};
