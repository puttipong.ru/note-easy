import { getConnectionManager,Connection,Repository,ObjectLiteral } from "typeorm";
import note_category from "../entities/note_category"

type getWhere = { uuid:string , categoryid:string }

export default class note_categoryRepository {
    private Repository: Repository<note_category> | ObjectLiteral
    private Manager: Connection
    
    constructor(nameDatabase: string = 'postgres') {
        this.Manager = getConnectionManager().get(nameDatabase);
        this.Repository = this.Manager.getRepository(note_category);
        
    }

    async get( id : string ,catid : string ):Promise<note_category>  {
        return this.Repository.findOne({ where: { uuid:id , categoryid:catid } as getWhere });
    }

    async getAll(id: string):Promise<note_category>  {
        return this.Repository.find({ where: { uuid:id } as getWhere });
    }
    
    async insert(request: note_category | note_category[]): Promise<note_category> {
        return this.Repository.insert(request);
    }

    async update(request: note_category | note_category[]):Promise<note_category>  {
        return this.Repository.save(request);
    }

    async delete(id:string,catid:string):Promise<note_category> {
        return this.Repository.delete({uuid:id,categoryid:catid});
    }

    async deleteAll(id : string):Promise<note_category> {
        return this.Repository.delete({ uuid:id });
    }
}