import { getConnectionManager,Connection,Repository,ObjectLiteral } from "typeorm";
import note_history from "../entities/note_history"

type getWhere = { noteid:string }

export default class note_historyRepository {
    private Repository: Repository<note_history> | ObjectLiteral
    private Manager: Connection
    
    constructor(nameDatabase: string = 'postgres') {
        this.Manager = getConnectionManager().get(nameDatabase);
        this.Repository = this.Manager.getRepository(note_history);
        
    }

    async get(id: string):Promise<note_history>  {
        return this.Repository.find({ where: { noteid:id } as getWhere });
    }
    
    async insert(request: note_history | note_history[]): Promise<note_history> {
        return this.Repository.insert(request);
    }

    async delete(id : string):Promise<note_history> {
        return this.Repository.delete({ noteid:id });
    }
}