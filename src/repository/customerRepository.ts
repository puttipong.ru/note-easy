import { getConnectionManager,Connection,Repository,ObjectLiteral } from "typeorm";
import customer from "../entities/customer"

type getWhere = { uuid:string , isDelete:boolean ,username:string}

export default class customerRepository {
    private Repository: Repository<customer> | ObjectLiteral
    private Manager: Connection
    
    constructor(nameDatabase: string = 'postgres') {
        this.Manager = getConnectionManager().get(nameDatabase);
        this.Repository = this.Manager.getRepository(customer);
        
    }

    async get(id: string):Promise<customer>  {
        return this.Repository.findOne({ where: { uuid:id} as getWhere });
    }

    async getid(user: string):Promise<customer>  {
        return this.Repository.findOne({ where: { username:user } as getWhere });
    }
    
    async insert(request: customer | customer[]): Promise<customer> {
        return this.Repository.insert(request);
    }

    async update(request: customer | customer[]):Promise<customer>  {
        return this.Repository.save(request);
    }

    async delete(request: customer):Promise<customer> {
        return this.Repository.delete(request);
    }
}