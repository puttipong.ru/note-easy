import { getConnectionManager,Connection,Repository,ObjectLiteral } from "typeorm";
import note from "../entities/note"

type getWhere = { uuid:string , noteid:string , isDeleted:boolean}

export default class noteRepository {
    private Repository: Repository<note> | ObjectLiteral
    private Manager: Connection
    
    constructor(nameDatabase: string = 'postgres') {
        this.Manager = getConnectionManager().get(nameDatabase);
        this.Repository = this.Manager.getRepository(note);
        
    }

    async get(id: string):Promise<note>  {
        return this.Repository.findOne({ where: { noteid:id } as getWhere });
    }

    async getAll(id: string):Promise<note>  {
        return this.Repository.find({ where: { uuid:id , isDeleted : false} as getWhere });
    }

    async getTrash(id: string):Promise<note>  {
        return this.Repository.find({ where: { uuid:id , isDeleted : true} as getWhere });
    }
    
    async insert(request: note | note[]): Promise<note> {
        return this.Repository.insert(request);
    }

    async update(request: note | note[]):Promise<note>  {
        return this.Repository.save(request);
    }

    async delete(request: note):Promise<note> {
        return this.Repository.delete(request);
    }

    async deleteAll(id : string):Promise<note> {
        return this.Repository.delete({ uuid:id });
    }

    async softDelete(request: note):Promise<note> {
        request.isDeleted = true;
        return this.Repository.save(request);
    }
    
    async restoreDelete(request: note):Promise<note> {
        request.isDeleted = false;
        return this.Repository.save(request);
    }

}