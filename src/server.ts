import { configServer, databaseConfig } from "./config";
import { Server } from "@hapi/hapi";
import dbConnection from "./logic/dbConnect"
import routes from "./routes/routeIndex";

export let server: Server;

export const init = async (): Promise<Server> => {
    server = new Server(configServer);
    dbConnection(databaseConfig)
    server.route(routes)
    server.ext({
        type: 'onPostAuth',
        method: (request, h) => {
            console.log('PATH');
            console.log(request.route.path);
            console.log('PARAMS')
            console.log(request.params)
            console.log('PAYLOAD')
            console.log(request.payload)
            return h.continue;
        }
    });
    server.ext({
        type: 'onPreHandler',
        method: (request, h) => {
            const _payload: any = request.payload
            try {
                let content:String = _payload.note_content
                const kw = require('./constant/badword.json')

                kw.forEach((element:any) => {
                    const replace = `${element}`;
                    const regx = new RegExp(replace,"g");
                    content = content.replace(regx,'***')        
                });
                _payload.note_content = content
                
            } catch {
                return h.continue
            }
            return h.continue;
        }
    });
    return server;
}

export const start = async (): Promise<void> => {
    console.log(`Listening on ${configServer.host}:${configServer.port}`);
    return server.start();
}

process.on('unhandledRejection', (err) => {
    console.error(err);
    process.exit(1);
})

