import { authJWT } from './../logic/authentication';
import { generateRandomDecimal } from './../logic/idGenerator';
import { noteCategoryDeleteValidation , noteCategoryCreateValidation, noteCategoryUpdateValidation } from './validation/note_categoryValidation';
import note_category from "../entities/note_category";
import note_categoryRepository from "../repository/note_categoryRepository";

export default {
    create: {
        path: '/category/create',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload

            const insert: note_category = {
                uuid: `${_payload.uuid}`,
                categoryid: generateRandomDecimal(16),
                categoryname: `${_payload.categoryname}`
            }
            const repo = new note_categoryRepository
            let result = "fail"

            try {
                await repo.insert(insert).then(() => { result = insert.categoryid })
                return h.response(result)
            } catch {
                return h.response(result)
            }

        },
        options: {
            validate: {
                payload: noteCategoryCreateValidation
            }
        }
    },
    update: {
        path: '/category/update',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload

            const update: note_category = {
                uuid: `${_payload.uuid}`,
                categoryid: `${_payload.categoryid}`,
                categoryname: `${_payload.categoryname}`
            }

            const repo = new note_categoryRepository
            let result = "fail"

            try {
                await repo.update(update).then(() => { result = "success" })
                return h.response(result)
            } catch {
                return h.response(result)
            }

        },
        options: {
            validate: {
                payload: noteCategoryUpdateValidation 
            }
        }
    },
    delete: {
        path: '/category/delete',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload
            const uuid = `${_payload.uuid}`
            const categoryid = `${_payload.categoryid}`
            const repo = new note_categoryRepository
            let result = "fail"

            try {
                await repo.delete(uuid, categoryid).then(() => { result = "success" })
                return h.response(result)
            } catch {
                return h.responst(result)
            }

        },
        options: {
            validate: {
                payload: noteCategoryDeleteValidation
            }
        }
    },
    get: {
        path: '/category',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload
            const uuid = `${_payload.uuid}`
            const categoryid = `${_payload.categoryid}`
            const repo = new note_categoryRepository
            let result = "fail"

            try {
                await repo.get(uuid, categoryid).then(() => { result = "success" })
                return h.response(result)
            } catch {
                return h.responst(result)
            }

        }
    },
    getall: {
        path: '/category/all',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload
            const uuid = `${_payload.uuid}`
            const repo = new note_categoryRepository
            let result = "fail"

            try {
                await repo.getAll(uuid).then(() => { result = "success" })
                return h.response(result)
            } catch {
                return h.responst(result)
            }

        }
    },
    
}
