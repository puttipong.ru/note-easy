import * as Joi from 'joi'

export const createNoteValidation = Joi.object({
    uuid : Joi.string().length(16),
    categoryid : Joi.string().optional().length(16),
    note_name : Joi.string().min(1).max(32),
    note_content : Joi.string().min(1).max(500)
})

export const updateNoteValidation = Joi.object({
    uuid : Joi.string().length(16),
    noteid : Joi.string().length(16),
    categoryid : Joi.string().optional().length(16),
    note_name : Joi.string().min(1).max(32),
    note_content : Joi.string().min(1).max(500)
})

export const getnoteValiation = Joi.object({
    noteid : Joi.string().length(16)
})

export const getnoteAllparamValidation = Joi.object({
    uuid : Joi.string().length(16)
})