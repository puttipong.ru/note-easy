import * as Joi from 'joi'

export const noteCategoryCreateValidation = Joi.object({
    uuid : Joi.string().length(16),
    categoryname : Joi.string().min(1).max(50)
})

export const noteCategoryUpdateValidation = Joi.object({
    uuid : Joi.string().length(16),
    categoryid : Joi.string().length(16),
    categoryname : Joi.string().min(1).max(32)
})

export const noteCategoryDeleteValidation = Joi.object({
    uuid : Joi.string().length(16),
    categoryid : Joi.string().length(16)
})
