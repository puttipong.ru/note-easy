import * as Joi from 'joi'

export const createCustomerValidation = Joi.object({
    username : Joi.string().min(3).max(32),
    password : Joi.string().min(8).max(32)
})

export const editCustomerValidation = Joi.object({
    uuid : Joi.string().length(16),
    username : Joi.string().min(3).max(32),
    password : Joi.string().min(8).max(32)
})

export const deleteCustomerValidation = Joi.object({
    uuid : Joi.string().length(16),
})

export const userParamsValidation = Joi.object({
    uuid : Joi.string().length(16),
})

export const loginValidation = Joi.object({
    username : Joi.string().min(3).max(32),
    password : Joi.string().min(8).max(32)
})

