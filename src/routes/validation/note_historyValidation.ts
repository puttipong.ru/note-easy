import * as Joi from 'joi'

export const insertNoteHistoryValidation = Joi.object({
    noteid : Joi.string().length(16),
    note_content : Joi.string().min(1).max(500)
})

export const noteHistoryValiation = Joi.object({
    noteid : Joi.string().length(16)
})
