import noteRoute from "./noteRoute"
import customerRoute from "./customerRoute"
import defaultRoute from "./defaultRoute"
import note_categoryRoute from "./note_categoryRoute"
import note_historyRoute from "./note_historyRoute"

let routes : any = []
routes.push(defaultRoute)
Object.entries(noteRoute).forEach(([_key, value]) =>{routes.push(value)})
Object.entries(customerRoute).forEach(([_key, value]) =>{routes.push(value)})
Object.entries(note_categoryRoute).forEach(([_key, value]) =>{routes.push(value)})
Object.entries(note_historyRoute).forEach(([_key, value]) =>{routes.push(value)})
export default routes