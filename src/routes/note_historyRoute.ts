import { authJWT } from './../logic/authentication';

import note_history from "../entities/note_history";
import note_historyRepository from "../repository/note_historyRepository"
import { insertNoteHistoryValidation , noteHistoryValiation } from "./validation/note_historyValidation";


export default {
    create: {
        path: '/note/history/insert',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload

            const insert: note_history = {
                noteid: `${_payload.noteid}`,
                note_content: `${_payload.note_content}`,
                timeStamp: new Date()
            }

            const repo = new note_historyRepository
            let result = "fail"
            try {
                await repo.insert(insert).then(() => { result = "success" })
                return h.response(result)
            } catch {
                return h.response(result)
            }
        },
        options:{
            validate:{
                payload: insertNoteHistoryValidation
            }
        }
    },
    delete: {
        path: '/note/history/delete',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload
            const repo = new note_historyRepository
            let result = "fail"
            try {
                await repo.delete(_payload.noteid).then(() => { result = "success" })
                return h.response(result)
            } catch {
                return h.response(result)
            }
        },
        options:{
            validate:{
                payload: noteHistoryValiation
            }
        }
    },
    getHistory: {
        path: `/note/history`,
        method: ['POST', 'PUT'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload
            const repo = new note_historyRepository
            const find = `${_payload.noteid}`
            let result
            try {
                await repo.get(find).then((data) => { result = data })
                return h.response(result)
            } catch {
                return h.response('fail')
            }
        },
        options:{
            validate:{
                payload: noteHistoryValiation
            }
        }
    },
}