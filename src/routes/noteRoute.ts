import { authJWT } from './../logic/authentication';
import { generateRandomDecimal } from './../logic/idGenerator';
import { getnoteValiation, updateNoteValidation, createNoteValidation, getnoteAllparamValidation } from './validation/noteValidation';
import note from "../entities/note";
import noteRepository from "../repository/noteRepository"

export default {
    create: {
        path: '/note/create',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload

            const insert: note = {
                uuid: `${_payload.uuid}`,
                noteid: generateRandomDecimal(16),
                categoryid: `${_payload.categoryid}`,
                note_name: `${_payload.note_name}`,
                note_content: `${_payload.note_content}`,
                isDeleted: false
            }

            const repo = new noteRepository
            let result = "fail"

            try {
                let jsondata
                await repo.insert(insert).then(() => { jsondata = insert.noteid   })
                return h.response(jsondata)
            } catch {
                return h.response(result)
            }
            
        },
        options: {
            validate: {
                payload: createNoteValidation
            }
        }
    },
    update: {
        path: '/note/update',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload
            const repo = new noteRepository

            let update: note = await repo.get(_payload.noteid)
            update.categoryid = `${_payload.categoryid}`
            update.note_name = `${_payload.note_name}`
            update.note_content = `${_payload.note_content}`

            let result = "fail"
            try {
                await repo.update(update).then(() => { result = "success" })
                return h.response(result)
            } catch {
                return h.response(result)
            }
        },
        options: {
            validate: {
                payload: updateNoteValidation
            }
        }
    },
    softDelete: {
        path: '/note/delete/soft',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload
            const repo = new noteRepository
            let softDelete: note = await repo.get(_payload.noteid)
            let result = "fail"
            try {
                await repo.softDelete(softDelete).then(() => { result = "success" })
                return h.response(result)
            } catch {
                return h.response(result)
            }
        },
        options: {
            validate: {
                payload: getnoteValiation
            }
        }
    },
    restoreDelete: {
        path: '/note/delete/restore',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload
            const repo = new noteRepository
            let restoreDelete: note = await repo.get(_payload.noteid)
            let result = "fail"
            try {
                await repo.restoreDelete(restoreDelete).then(() => { result = "success" })
                return h.response(result)
            } catch {
                return h.response(result)
            }
        },
        options: {
            validate: {
                payload: getnoteValiation
            }
        }
    },
    permDelete: {
        path: '/note/delete/perm',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload
            const repo = new noteRepository
            let permDelete: note = await repo.get(_payload.noteid)
            let result = "fail"
            try {
                await repo.delete(permDelete).then(() => { result = "success" })
                return h.response(result)
            } catch {
                return h.response(result)
            }
        },
        options: {
            validate: {
                payload: getnoteValiation
            }
        }
    },
    getNote: {
        path: `/note/{noteid}`,
        method: ['GET'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const repo = new noteRepository
            const find = `${request.params.noteid}`
            let result
            try {
                await repo.get(find).then((data) => { result = data })
                return h.send(result)
            } catch {
                return h.response('fail to find note')
            }
        },
        options: {
            validate: {
                params: getnoteValiation
            }
        }
    },
    getAllNote: {
        path: `/{uuid}/notes`,
        method: ['GET'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const repo = new noteRepository
            const find = `${request.params.uuid}`
            console.log(find);
            let result = {}
            try {
                await repo.getAll(find).then(data => result = data )
                return h.response(result)
            } catch {
                return h.response('fail')
            }
        },
        options: {
            validate: {
                params: getnoteAllparamValidation
            }
        }
    }
}