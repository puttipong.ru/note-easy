import { authJWT, signtoken, validatepassword } from './../logic/authentication';
import { hashPassword } from '../logic/authentication';
import { createCustomerValidation, deleteCustomerValidation, editCustomerValidation, userParamsValidation, loginValidation } from './validation/customerValidation';
import { generateRandomHex } from './../logic/idGenerator';
import customer from "../entities/customer";
import customerRepository from "../repository/customerRepository"

export default {
    create: {
        path: '/customer/create',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            const _payload: any = request.payload
            const repo = new customerRepository
            
            let data = await repo.getid(_payload.username)
            if (data != undefined) {
                return h.response('user already existed')
            }
            
            const pwd = hashPassword(_payload.password)
            const insert: customer = {
                uuid: generateRandomHex(16),
                username: `${_payload.username}`,
                hash:pwd.hash,
                salt:pwd.salt
            }
            let fail = true
            while(fail){
            try {
                let data = ''
                await repo.insert(insert).then(() => { 
                    fail=false;
                    data = 'create user success'
                })
                return h.response(data)
            } catch {
                insert.uuid = generateRandomHex(16)
                continue
            }
        }

        },
        options: {
            validate: {
                payload: createCustomerValidation
            }
        }
    },
    update: {
        path: '/customer/edit',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload
            const pwd = await hashPassword(_payload.password)

            const update: customer = {
                uuid: `${_payload.uuid}`,
                username: `${_payload.username}`,
                hash:pwd.hash,
                salt:pwd.salt
            }

            const repo = new customerRepository
            let result = "fail"

            try {
                await repo.update(update).then(() => { result = "success" })
                return h.response(result)
            } catch {
                return h.response(result)
            }

        },
        options: {
            validate: {
                payload: editCustomerValidation
            }
        }
    },
    delete: {
        path: '/customer/deactivate',
        method: ['PUT', 'POST'],
        handler: async (request: any, h: any) => {
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const _payload: any = request.payload
            const repo = new customerRepository
            let toDelete: customer = await repo.get(_payload.uuid)
            let result = "fail"

            try {
                await repo.delete(toDelete).then(() => { result = "success" })
                return h.response(result)
            } catch {
                return h.response(result)
            }

        },
        options: {
            validate: {
                payload: deleteCustomerValidation
            }
        }
    },
    getUser: {
        path: `/customer/{uuid}`,
        method: ['GET'],
        handler: async (request: any, h: any) => {   
            try{
                authJWT(request)
            }catch{
                return h.response('invalid token')
            }
            const repo = new customerRepository
            const id = request.params.uuid
            const find = `${id}`
            let result

            try {
                await repo.get(find).then((data) => { result = data.username})
                return h.response(result)
            } catch {
                return h.response('fail')
            }

        },
        options: {
            validate: {
                params: userParamsValidation
            }
        }
    },
    login: {
        path: `/customer/login`,
        method: ['POST'],
        handler: async (request: any, h: any) => {
            const _payload: any = request.payload
            const repo = new customerRepository
            const find = `${_payload.username}`
            let result = ''
            try {
                let token = ''
                let dataobject : any
                await repo.getid(find).then((data) => { 
                    if(validatepassword( _payload.password , data.salt , data.hash )){
                        token = signtoken({ uid:data.uuid , user:data.username })
                        dataobject = data
                    }else{
                        dataobject = ''
                    }    
                })
                return h.response({token:token,uuid:dataobject.uuid})
            } catch {
                result = 'cannot find user'
                return h.response(result)
            }

        },
        options: {
            validate: {
                payload: loginValidation
            }
        }
    },

}
