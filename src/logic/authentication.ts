import { secretkey } from './../config';
import * as crypto from 'crypto'
import * as jwt from 'jsonwebtoken'

export const hashPassword = (password: string) => {
  let salt = crypto.randomBytes(16).toString('hex')
  let hash = crypto.pbkdf2Sync(password, salt, 1000, 64, `sha512`).toString(`hex`)
  return { salt, hash }
}

export const validatepassword = (password: string, salt: string, hashToValid: string) => {
  let hash = crypto.pbkdf2Sync(password, salt, 1000, 64, `sha512`).toString(`hex`)
  return hashToValid === hash
};

export const signtoken = (datajson: any) => {
  const signoption = {
    expiresIn: '1h'
  }
  let token = jwt.sign(datajson, secretkey, signoption)
  // let token = jwt.sign(datajson,secretkey)
  return token
}

export const authJWT = (request: any) => {
  const authHeader = request.headers['authorization']
  const token = authHeader.split(' ')[1]
  const decoded :any =jwt.verify(token, secretkey)
  return decoded.uuid
}

