const Deci = "0123456789"
const Hex = "0123456789abcedf"

export const generateRandomDecimal = (length: number) => {
    let result = ""
    const charactersLength = Deci.length
    for (var i = 0; i < length; i++) {
        result += Deci.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result
}

export const generateRandomHex = (length: number) => {
    let result = ""
    const charactersLength = Hex.length
    for (var i = 0; i < length; i++) {
        result += Hex.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result
}