import { createConnection,Connection } from "typeorm";
import Model from '../entities/index';

export default async(dbConfig:any):Promise<Connection> => {
    dbConfig.entities = Model ;
    return createConnection(dbConfig);
}