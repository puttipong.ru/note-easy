"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const note_1 = require("./note");
const customer_1 = require("./customer");
const note_history_1 = require("./note_history");
const note_category_1 = require("./note_category");
exports.default = [note_1.default, note_history_1.default, customer_1.default, note_category_1.default];
//# sourceMappingURL=index.js.map