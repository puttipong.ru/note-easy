"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
let note_history = class note_history {
};
__decorate([
    (0, typeorm_1.PrimaryColumn)({
        length: 16,
        nullable: false
    }),
    __metadata("design:type", String)
], note_history.prototype, "noteid", void 0);
__decorate([
    (0, typeorm_1.Column)({ nullable: true }),
    __metadata("design:type", String)
], note_history.prototype, "note_content", void 0);
__decorate([
    (0, typeorm_1.PrimaryColumn)({ type: "timestamp" }),
    __metadata("design:type", Date)
], note_history.prototype, "timeStamp", void 0);
note_history = __decorate([
    (0, typeorm_1.Entity)({ name: 'note_history' })
], note_history);
exports.default = note_history;
;
//# sourceMappingURL=note_history.js.map