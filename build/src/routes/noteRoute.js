"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const authentication_1 = require("./../logic/authentication");
const idGenerator_1 = require("./../logic/idGenerator");
const noteValidation_1 = require("./validation/noteValidation");
const noteRepository_1 = require("../repository/noteRepository");
exports.default = {
    create: {
        path: '/note/create',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_a) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const insert = {
                uuid: `${_payload.uuid}`,
                noteid: (0, idGenerator_1.generateRandomDecimal)(16),
                categoryid: `${_payload.categoryid}`,
                note_name: `${_payload.note_name}`,
                note_content: `${_payload.note_content}`,
                isDeleted: false
            };
            const repo = new noteRepository_1.default;
            let result = "fail";
            try {
                let jsondata;
                yield repo.insert(insert).then(() => { jsondata = insert.noteid; });
                return h.response(jsondata);
            }
            catch (_b) {
                return h.response(result);
            }
        }),
        options: {
            validate: {
                payload: noteValidation_1.createNoteValidation
            }
        }
    },
    update: {
        path: '/note/update',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_c) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const repo = new noteRepository_1.default;
            let update = yield repo.get(_payload.noteid);
            update.categoryid = `${_payload.categoryid}`;
            update.note_name = `${_payload.note_name}`;
            update.note_content = `${_payload.note_content}`;
            let result = "fail";
            try {
                yield repo.update(update).then(() => { result = "success"; });
                return h.response(result);
            }
            catch (_d) {
                return h.response(result);
            }
        }),
        options: {
            validate: {
                payload: noteValidation_1.updateNoteValidation
            }
        }
    },
    softDelete: {
        path: '/note/delete/soft',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_e) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const repo = new noteRepository_1.default;
            let softDelete = yield repo.get(_payload.noteid);
            let result = "fail";
            try {
                yield repo.softDelete(softDelete).then(() => { result = "success"; });
                return h.response(result);
            }
            catch (_f) {
                return h.response(result);
            }
        }),
        options: {
            validate: {
                payload: noteValidation_1.getnoteValiation
            }
        }
    },
    restoreDelete: {
        path: '/note/delete/restore',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_g) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const repo = new noteRepository_1.default;
            let restoreDelete = yield repo.get(_payload.noteid);
            let result = "fail";
            try {
                yield repo.restoreDelete(restoreDelete).then(() => { result = "success"; });
                return h.response(result);
            }
            catch (_h) {
                return h.response(result);
            }
        }),
        options: {
            validate: {
                payload: noteValidation_1.getnoteValiation
            }
        }
    },
    permDelete: {
        path: '/note/delete/perm',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_j) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const repo = new noteRepository_1.default;
            let permDelete = yield repo.get(_payload.noteid);
            let result = "fail";
            try {
                yield repo.delete(permDelete).then(() => { result = "success"; });
                return h.response(result);
            }
            catch (_k) {
                return h.response(result);
            }
        }),
        options: {
            validate: {
                payload: noteValidation_1.getnoteValiation
            }
        }
    },
    getNote: {
        path: `/note/{noteid}`,
        method: ['GET'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_l) {
                return h.response('invalid token');
            }
            const repo = new noteRepository_1.default;
            const find = `${request.params.noteid}`;
            let result;
            try {
                yield repo.get(find).then((data) => { result = data; });
                return h.send(result);
            }
            catch (_m) {
                return h.response('fail to find note');
            }
        }),
        options: {
            validate: {
                params: noteValidation_1.getnoteValiation
            }
        }
    },
    getAllNote: {
        path: `/{uuid}/notes`,
        method: ['GET'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_o) {
                return h.response('invalid token');
            }
            const repo = new noteRepository_1.default;
            const find = `${request.params.uuid}`;
            console.log(find);
            let result = {};
            try {
                yield repo.getAll(find).then(data => result = data);
                return h.response(result);
            }
            catch (_p) {
                return h.response('fail');
            }
        }),
        options: {
            validate: {
                params: noteValidation_1.getnoteAllparamValidation
            }
        }
    }
};
//# sourceMappingURL=noteRoute.js.map