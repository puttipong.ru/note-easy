"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const noteRoute_1 = require("./noteRoute");
const customerRoute_1 = require("./customerRoute");
const defaultRoute_1 = require("./defaultRoute");
const note_categoryRoute_1 = require("./note_categoryRoute");
const note_historyRoute_1 = require("./note_historyRoute");
let routes = [];
routes.push(defaultRoute_1.default);
Object.entries(noteRoute_1.default).forEach(([_key, value]) => { routes.push(value); });
Object.entries(customerRoute_1.default).forEach(([_key, value]) => { routes.push(value); });
Object.entries(note_categoryRoute_1.default).forEach(([_key, value]) => { routes.push(value); });
Object.entries(note_historyRoute_1.default).forEach(([_key, value]) => { routes.push(value); });
exports.default = routes;
//# sourceMappingURL=routeIndex.js.map