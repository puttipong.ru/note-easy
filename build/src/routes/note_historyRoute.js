"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const authentication_1 = require("./../logic/authentication");
const note_historyRepository_1 = require("../repository/note_historyRepository");
const note_historyValidation_1 = require("./validation/note_historyValidation");
exports.default = {
    create: {
        path: '/note/history/insert',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_a) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const insert = {
                noteid: `${_payload.noteid}`,
                note_content: `${_payload.note_content}`,
                timeStamp: new Date()
            };
            const repo = new note_historyRepository_1.default;
            let result = "fail";
            try {
                yield repo.insert(insert).then(() => { result = "success"; });
                return h.response(result);
            }
            catch (_b) {
                return h.response(result);
            }
        }),
        options: {
            validate: {
                payload: note_historyValidation_1.insertNoteHistoryValidation
            }
        }
    },
    delete: {
        path: '/note/history/delete',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_c) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const repo = new note_historyRepository_1.default;
            let result = "fail";
            try {
                yield repo.delete(_payload.noteid).then(() => { result = "success"; });
                return h.response(result);
            }
            catch (_d) {
                return h.response(result);
            }
        }),
        options: {
            validate: {
                payload: note_historyValidation_1.noteHistoryValiation
            }
        }
    },
    getHistory: {
        path: `/note/history`,
        method: ['POST', 'PUT'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_e) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const repo = new note_historyRepository_1.default;
            const find = `${_payload.noteid}`;
            let result;
            try {
                yield repo.get(find).then((data) => { result = data; });
                return h.response(result);
            }
            catch (_f) {
                return h.response('fail');
            }
        }),
        options: {
            validate: {
                payload: note_historyValidation_1.noteHistoryValiation
            }
        }
    },
};
//# sourceMappingURL=note_historyRoute.js.map