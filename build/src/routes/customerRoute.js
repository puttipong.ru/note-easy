"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const authentication_1 = require("./../logic/authentication");
const authentication_2 = require("../logic/authentication");
const customerValidation_1 = require("./validation/customerValidation");
const idGenerator_1 = require("./../logic/idGenerator");
const customerRepository_1 = require("../repository/customerRepository");
exports.default = {
    create: {
        path: '/customer/create',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            const _payload = request.payload;
            const repo = new customerRepository_1.default;
            let data = yield repo.getid(_payload.username);
            if (data != undefined) {
                return h.response('user already existed');
            }
            const pwd = (0, authentication_2.hashPassword)(_payload.password);
            const insert = {
                uuid: (0, idGenerator_1.generateRandomHex)(16),
                username: `${_payload.username}`,
                hash: pwd.hash,
                salt: pwd.salt
            };
            let fail = true;
            while (fail) {
                try {
                    let data = '';
                    yield repo.insert(insert).then(() => {
                        fail = false;
                        data = 'create user success';
                    });
                    return h.response(data);
                }
                catch (_a) {
                    insert.uuid = (0, idGenerator_1.generateRandomHex)(16);
                    continue;
                }
            }
        }),
        options: {
            validate: {
                payload: customerValidation_1.createCustomerValidation
            }
        }
    },
    update: {
        path: '/customer/edit',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_b) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const pwd = yield (0, authentication_2.hashPassword)(_payload.password);
            const update = {
                uuid: `${_payload.uuid}`,
                username: `${_payload.username}`,
                hash: pwd.hash,
                salt: pwd.salt
            };
            const repo = new customerRepository_1.default;
            let result = "fail";
            try {
                yield repo.update(update).then(() => { result = "success"; });
                return h.response(result);
            }
            catch (_c) {
                return h.response(result);
            }
        }),
        options: {
            validate: {
                payload: customerValidation_1.editCustomerValidation
            }
        }
    },
    delete: {
        path: '/customer/deactivate',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_d) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const repo = new customerRepository_1.default;
            let toDelete = yield repo.get(_payload.uuid);
            let result = "fail";
            try {
                yield repo.delete(toDelete).then(() => { result = "success"; });
                return h.response(result);
            }
            catch (_e) {
                return h.response(result);
            }
        }),
        options: {
            validate: {
                payload: customerValidation_1.deleteCustomerValidation
            }
        }
    },
    getUser: {
        path: `/customer/{uuid}`,
        method: ['GET'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_f) {
                return h.response('invalid token');
            }
            const repo = new customerRepository_1.default;
            const id = request.params.uuid;
            const find = `${id}`;
            let result;
            try {
                yield repo.get(find).then((data) => { result = data.username; });
                return h.response(result);
            }
            catch (_g) {
                return h.response('fail');
            }
        }),
        options: {
            validate: {
                params: customerValidation_1.userParamsValidation
            }
        }
    },
    login: {
        path: `/customer/login`,
        method: ['POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            const _payload = request.payload;
            const repo = new customerRepository_1.default;
            const find = `${_payload.username}`;
            let result = '';
            try {
                let token = '';
                let dataobject;
                yield repo.getid(find).then((data) => {
                    if ((0, authentication_1.validatepassword)(_payload.password, data.salt, data.hash)) {
                        token = (0, authentication_1.signtoken)({ uid: data.uuid, user: data.username });
                        dataobject = data;
                    }
                    else {
                        dataobject = '';
                    }
                });
                return h.response({ token: token, uuid: dataobject.uuid });
            }
            catch (_h) {
                result = 'cannot find user';
                return h.response(result);
            }
        }),
        options: {
            validate: {
                payload: customerValidation_1.loginValidation
            }
        }
    },
};
//# sourceMappingURL=customerRoute.js.map