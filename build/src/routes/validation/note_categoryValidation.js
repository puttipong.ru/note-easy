"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.noteCategoryDeleteValidation = exports.noteCategoryUpdateValidation = exports.noteCategoryCreateValidation = void 0;
const Joi = require("joi");
exports.noteCategoryCreateValidation = Joi.object({
    uuid: Joi.string().length(16),
    categoryname: Joi.string().min(1).max(50)
});
exports.noteCategoryUpdateValidation = Joi.object({
    uuid: Joi.string().length(16),
    categoryid: Joi.string().length(16),
    categoryname: Joi.string().min(1).max(32)
});
exports.noteCategoryDeleteValidation = Joi.object({
    uuid: Joi.string().length(16),
    categoryid: Joi.string().length(16)
});
//# sourceMappingURL=note_categoryValidation.js.map