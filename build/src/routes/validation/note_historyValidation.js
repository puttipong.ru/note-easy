"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.noteHistoryValiation = exports.insertNoteHistoryValidation = void 0;
const Joi = require("joi");
exports.insertNoteHistoryValidation = Joi.object({
    noteid: Joi.string().length(16),
    note_content: Joi.string().min(1).max(500)
});
exports.noteHistoryValiation = Joi.object({
    noteid: Joi.string().length(16)
});
//# sourceMappingURL=note_historyValidation.js.map