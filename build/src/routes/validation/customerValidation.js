"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.loginValidation = exports.userParamsValidation = exports.deleteCustomerValidation = exports.editCustomerValidation = exports.createCustomerValidation = void 0;
const Joi = require("joi");
exports.createCustomerValidation = Joi.object({
    username: Joi.string().min(3).max(32),
    password: Joi.string().min(8).max(32)
});
exports.editCustomerValidation = Joi.object({
    uuid: Joi.string().length(16),
    username: Joi.string().min(3).max(32),
    password: Joi.string().min(8).max(32)
});
exports.deleteCustomerValidation = Joi.object({
    uuid: Joi.string().length(16),
});
exports.userParamsValidation = Joi.object({
    uuid: Joi.string().length(16),
});
exports.loginValidation = Joi.object({
    username: Joi.string().min(3).max(32),
    password: Joi.string().min(8).max(32)
});
//# sourceMappingURL=customerValidation.js.map