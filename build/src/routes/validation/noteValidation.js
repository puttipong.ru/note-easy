"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getnoteAllparamValidation = exports.getnoteValiation = exports.updateNoteValidation = exports.createNoteValidation = void 0;
const Joi = require("joi");
exports.createNoteValidation = Joi.object({
    uuid: Joi.string().length(16),
    categoryid: Joi.string().optional().length(16),
    note_name: Joi.string().min(1).max(32),
    note_content: Joi.string().min(1).max(500)
});
exports.updateNoteValidation = Joi.object({
    uuid: Joi.string().length(16),
    noteid: Joi.string().length(16),
    categoryid: Joi.string().optional().length(16),
    note_name: Joi.string().min(1).max(32),
    note_content: Joi.string().min(1).max(500)
});
exports.getnoteValiation = Joi.object({
    noteid: Joi.string().length(16)
});
exports.getnoteAllparamValidation = Joi.object({
    uuid: Joi.string().length(16)
});
//# sourceMappingURL=noteValidation.js.map