"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const authentication_1 = require("./../logic/authentication");
const idGenerator_1 = require("./../logic/idGenerator");
const note_categoryValidation_1 = require("./validation/note_categoryValidation");
const note_categoryRepository_1 = require("../repository/note_categoryRepository");
exports.default = {
    create: {
        path: '/category/create',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_a) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const insert = {
                uuid: `${_payload.uuid}`,
                categoryid: (0, idGenerator_1.generateRandomDecimal)(16),
                categoryname: `${_payload.categoryname}`
            };
            const repo = new note_categoryRepository_1.default;
            let result = "fail";
            try {
                yield repo.insert(insert).then(() => { result = insert.categoryid; });
                return h.response(result);
            }
            catch (_b) {
                return h.response(result);
            }
        }),
        options: {
            validate: {
                payload: note_categoryValidation_1.noteCategoryCreateValidation
            }
        }
    },
    update: {
        path: '/category/update',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_c) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const update = {
                uuid: `${_payload.uuid}`,
                categoryid: `${_payload.categoryid}`,
                categoryname: `${_payload.categoryname}`
            };
            const repo = new note_categoryRepository_1.default;
            let result = "fail";
            try {
                yield repo.update(update).then(() => { result = "success"; });
                return h.response(result);
            }
            catch (_d) {
                return h.response(result);
            }
        }),
        options: {
            validate: {
                payload: note_categoryValidation_1.noteCategoryUpdateValidation
            }
        }
    },
    delete: {
        path: '/category/delete',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_e) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const uuid = `${_payload.uuid}`;
            const categoryid = `${_payload.categoryid}`;
            const repo = new note_categoryRepository_1.default;
            let result = "fail";
            try {
                yield repo.delete(uuid, categoryid).then(() => { result = "success"; });
                return h.response(result);
            }
            catch (_f) {
                return h.responst(result);
            }
        }),
        options: {
            validate: {
                payload: note_categoryValidation_1.noteCategoryDeleteValidation
            }
        }
    },
    get: {
        path: '/category',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_g) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const uuid = `${_payload.uuid}`;
            const categoryid = `${_payload.categoryid}`;
            const repo = new note_categoryRepository_1.default;
            let result = "fail";
            try {
                yield repo.get(uuid, categoryid).then(() => { result = "success"; });
                return h.response(result);
            }
            catch (_h) {
                return h.responst(result);
            }
        })
    },
    getall: {
        path: '/category/all',
        method: ['PUT', 'POST'],
        handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
            try {
                (0, authentication_1.authJWT)(request);
            }
            catch (_j) {
                return h.response('invalid token');
            }
            const _payload = request.payload;
            const uuid = `${_payload.uuid}`;
            const repo = new note_categoryRepository_1.default;
            let result = "fail";
            try {
                yield repo.getAll(uuid).then(() => { result = "success"; });
                return h.response(result);
            }
            catch (_k) {
                return h.responst(result);
            }
        })
    },
};
//# sourceMappingURL=note_categoryRoute.js.map