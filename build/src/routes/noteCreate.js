"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const noteRepository_1 = require("../db/noteRepository");
exports.default = {
    path: '/db',
    method: ['PUT', 'POST'],
    handler: (request, h) => __awaiter(void 0, void 0, void 0, function* () {
        const _payload = request.payload;
        const insert = {
            uuid: `${_payload.uuid}`,
            noteid: `${_payload.noteid}`,
            categoryid: '1',
            note_name: `${_payload.note_name}`,
            note_content: `${_payload.note_content}`,
            isDeleted: false
        };
        const repo = new noteRepository_1.default;
        let result = "fail";
        yield repo.insert(insert).then(() => { result = "success"; });
        return h.response(result);
    })
};
//# sourceMappingURL=noteCreate.js.map