"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authJWT = exports.signtoken = exports.validatepassword = exports.hashPassword = void 0;
const config_1 = require("./../config");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const hashPassword = (password) => {
    let salt = crypto.randomBytes(16).toString('hex');
    let hash = crypto.pbkdf2Sync(password, salt, 1000, 64, `sha512`).toString(`hex`);
    return { salt, hash };
};
exports.hashPassword = hashPassword;
const validatepassword = (password, salt, hashToValid) => {
    let hash = crypto.pbkdf2Sync(password, salt, 1000, 64, `sha512`).toString(`hex`);
    return hashToValid === hash;
};
exports.validatepassword = validatepassword;
const signtoken = (datajson) => {
    const signoption = {
        expiresIn: '1h'
    };
    let token = jwt.sign(datajson, config_1.secretkey, signoption);
    return token;
};
exports.signtoken = signtoken;
const authJWT = (request) => {
    const authHeader = request.headers['authorization'];
    const token = authHeader.split(' ')[1];
    const decoded = jwt.verify(token, config_1.secretkey);
    return decoded.uuid;
};
exports.authJWT = authJWT;
//# sourceMappingURL=authentication.js.map