"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateRandomHex = exports.generateRandomDecimal = void 0;
const Deci = "0123456789";
const Hex = "0123456789abcedf";
const generateRandomDecimal = (length) => {
    let result = "";
    const charactersLength = Deci.length;
    for (var i = 0; i < length; i++) {
        result += Deci.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
};
exports.generateRandomDecimal = generateRandomDecimal;
const generateRandomHex = (length) => {
    let result = "";
    const charactersLength = Hex.length;
    for (var i = 0; i < length; i++) {
        result += Hex.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
};
exports.generateRandomHex = generateRandomHex;
//# sourceMappingURL=idGenerator.js.map