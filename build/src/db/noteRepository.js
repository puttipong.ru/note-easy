"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const note_1 = require("../entities/note");
class noteRepository {
    constructor(nameDatabase = 'postgres') {
        this.Manager = (0, typeorm_1.getConnectionManager)().get(nameDatabase);
        this.Repository = this.Manager.getRepository(note_1.default);
    }
    get(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.Repository.findOne({ where: { uuid: id } });
        });
    }
    insert(request) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.Repository.insert(request);
        });
    }
    update(request) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.Repository.save(request);
        });
    }
    delete(request) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.Repository.delete(request);
        });
    }
}
exports.default = noteRepository;
//# sourceMappingURL=noteRepository.js.map