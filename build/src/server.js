"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.start = exports.init = exports.server = void 0;
const config_1 = require("./config");
const hapi_1 = require("@hapi/hapi");
const dbConnect_1 = require("./logic/dbConnect");
const routeIndex_1 = require("./routes/routeIndex");
const init = () => __awaiter(void 0, void 0, void 0, function* () {
    exports.server = new hapi_1.Server(config_1.configServer);
    (0, dbConnect_1.default)(config_1.databaseConfig);
    exports.server.route(routeIndex_1.default);
    exports.server.ext({
        type: 'onPostAuth',
        method: (request, h) => {
            console.log('PATH');
            console.log(request.route.path);
            console.log('PARAMS');
            console.log(request.params);
            console.log('PAYLOAD');
            console.log(request.payload);
            return h.continue;
        }
    });
    exports.server.ext({
        type: 'onPreHandler',
        method: (request, h) => {
            const _payload = request.payload;
            try {
                let content = _payload.note_content;
                const kw = require('./constant/badword.json');
                kw.forEach((element) => {
                    const replace = `${element}`;
                    const regx = new RegExp(replace, "g");
                    content = content.replace(regx, '***');
                });
                _payload.note_content = content;
            }
            catch (_a) {
                return h.continue;
            }
            return h.continue;
        }
    });
    return exports.server;
});
exports.init = init;
const start = () => __awaiter(void 0, void 0, void 0, function* () {
    console.log(`Listening on ${config_1.configServer.host}:${config_1.configServer.port}`);
    return exports.server.start();
});
exports.start = start;
process.on('unhandledRejection', (err) => {
    console.error(err);
    process.exit(1);
});
//# sourceMappingURL=server.js.map