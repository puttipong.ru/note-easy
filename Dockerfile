FROM node:14.18.3-alpine3.15

WORKDIR /app
COPY package.json ./
RUN npm install --production
COPY build/src ./
EXPOSE 5566
CMD ["npm","run","start"]